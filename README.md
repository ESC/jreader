
A simple demo suite exploring ROOT input/output option(s) for JANA
==================================================================

  A basic ROOT tree is imported in several single-threaded
and multi-threaded ways. An output 1D histogram is created 
and gets written into the output ROOT file. 

  More advanced plugin case demonstrates how to store JEvent
objects in a ROOT file and retrieve them later on.

  A minor modification of Jana codes is required (see jana-2.0.0dev.patch
file for the details). No ROOT dependency is introduced (but it is 
assumed that JObject class data, and in particular JObject::associated,
never change).

  The stuff only works for objects inheriting from XOjbect base
class (to have it working for any JObject derivative class one 
needs to resolve a conflict with JFactoryT::Insert() overloading).
This is a minor thing I guess.

  But in addition to this, JEvent::InsertXObject() method should be called 
instead of the JEvent::Insert(), when populating JEvents. A more clean 
(templated) implementation may exist. I did not find it though, so a 
poor man's solution is implemented in this demo version. 

  Apparently this is a demo only (no memory cleanup, brutal ignorance 
of 'const' modifiers, etc). But it does seem to serve the purpose: one can
implement generic enough ROOT-based check-pointing (as well as perhaps 
a persistency model) for Jana. See XEvent class - it does not know 
anything about the actual contents of the imported/exported JEvents.
One has to list all the classes in the ROOT dictionary of course, but 
not the *way* these class instances will be present in the JEvents.


Compile
-------

```
  # Export ROOTSYS first!; the code was checked with 6.14.00 & 6.18.00;
  #
  mkdir build && cd build
  cmake -DJANA=<jana-installation-directory> -Wno-dev ..
  make -j4
  export JANA_PLUGIN_PATH=${PWD}
```

Create input tree
-----------------

```
  ./bootstrap  # a simple 'input.root' ROOT tree with 10^6 double entries
```

Read the tree (1)
-----------------

```  
  All of these create 'output.root' file with a single histogram 'h1', 
but a TTree input loop is done in a very different way.

  reader* executables are standalone loops in ROOT environment

  libjreader is a JANA plugin
```

```
  ./reader1 # straightforward single-threaded loop
```
```
  ./reader2 # 4 threads using TTreeProcessorMT interface; does not work in 6.18.00; hmm;
```
```
  ./reader3 # 4 threads using std::thread interface manually
```
```
  ./reader4 # 4 threads using RDataFrame interface
```

```
  # JANA loop with TH1D::Fill() called right from the GetEvent() method;
  # multi-threading does not give any real gain here (as expected), since
  # GetEvent() can not be run concurrently in Jana, while Process() call is a dummy; 
  jana -Pplugins=libjreader -Pnthreads=4 /dev/zero
```

Plot the output histogram (1)
-----------------------------

```
  # Displays a single 'h1' histogram with 10^6 entries created after any of the 
  # passes above; 
  #
  # all the histograms should be identical (and they are);
  root -l ../plotter.C
```

Read the tree (2)
-----------------

```  
  All of these are Jana-based, with a Processor() method activated; they create 
'output.root' file with a single histogram 'h1', but also a TTree with the JEvent factory dump.

 # Direct conversion "input.root" -> "output.root"
jana -Pplugins=libjreader -Pstatistics=100000 -Pnthreads=4 -Pwith-processor=yes -Pinput-queue-depth=100 -Poutput-queue-depth=10 -Puse-checkpoint=no /dev/zero

 # Conversion "input.root" -> "output.root" by re-using JEvent factory state of the previous pass;
jana -Pplugins=libjreader -Pstatistics=100000 -Pnthreads=4 -Pwith-processor=yes -Pinput-queue-depth=100 -Poutput-queue-depth=10 -Puse-checkpoint=yes /dev/zero

 # A more fair comparison of single-threaded vs multi-threaded case (~0.1ms nanosleep in the Processor() call);
jana -Pplugins=libjreader -Pstatistics=100000 -Pnthreads=1 -Pwith-processor=yes -Pinput-queue-depth=100 -Poutput-queue-depth=10 -Puse-checkpoint=yes -Pfake-cpu-load=100000 /dev/zero

jana -Pplugins=libjreader -Pstatistics=100000 -Pnthreads=4 -Pwith-processor=yes -Pinput-queue-depth=100 -Poutput-queue-depth=10 -Puse-checkpoint=yes -Pfake-cpu-load=100000 /dev/zero

  Apparently one can play with the parameters, but perhaps also use more realistic output event structures.

```

Plot the output histogram (2)
-----------------------------

```
  # Displays four 1D histograms with 10^5 entries, as created by any of the 
  # commands above:
  #
  # (1) created using ClassOne right in the Processor() method, and just stored in 
  # the "output.root" file; (2) extracted from ClassOne entry in JEvent dump; 
  # (3) extracted from ClassTwo entry in JEvent dump; (4) extracted from 
  # ClassOne associate of ClassTwo entry in JEvent dump;
  #
  # all the histograms should be identical (and they are);
  root -l ../plotter.C
```

