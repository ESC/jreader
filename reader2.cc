
#include <TFile.h>
#include <TH1D.h>
#include <ROOT/TTreeProcessorMT.hxx>

int main( void )
{
  ROOT::EnableImplicitMT(4);

  // Initialize TTreeProcessorMT and then let it do the job;
  ROOT::TTreeProcessorMT tp("input.root", "tree"); 

  TFile fout("output.root", "RECREATE");

  ROOT::TThreadedObject<TH1D> h1("h1", "h1", 1000, -10., 10.);
  
  auto myFunction = [&](TTreeReader &myReader) {
    TTreeReaderValue<double> value(myReader, "value");

    auto myH1Hist = h1.Get();

    while (myReader.Next()) myH1Hist->Fill(*value);
  };
  
  tp.Process(myFunction);

  // It looks like h1->Write() call performs Merge() internally;
  //auto h1Merged = h1.Merge();
  h1->Write(); fout.Close();
} // main()
