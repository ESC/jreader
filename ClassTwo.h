
#include <vector>

#include <TObject.h>

#include <JANA/JObject.h>

#ifndef _CLASS_TWO_
#define _CLASS_TWO_

class ClassTwo: public XObject , public TObject {
 public:
  ClassTwo() {};
  ClassTwo(double value) { mValues.push_back(value); };

  double GetValue(unsigned iq) const { return (iq < mValues.size() ? mValues[iq] : 0.0); };

 private:
  // Just for fun make it differently compared to ClassOne;
  std::vector<double> mValues;

  ClassDef(ClassTwo,1);
};

#endif
