
#ifdef __CINT__

#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;

//#pragma link C++ options=version(0) class JObject+;
//#pragma link C++ options=version(0) class XObject+;
#pragma link C++ class JObject+;
#pragma link C++ class XObject+;

// Added this line for ROOT 6.18.00; was not needed in ROOT 6.14.00; hmm;
// ok, this missing line was exactly the reason why the code crashed in 
// the check-pointed multi-threaded case; 
#pragma link C++ class std::vector<JObject*>+;

#pragma link C++ class ClassOne+;
#pragma link C++ class ClassTwo+;

#pragma link C++ class XEvent+;

#endif
