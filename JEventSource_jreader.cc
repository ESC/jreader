
#include "JEventSource_jreader.h"
#include "JEventProcessor_jreader.h"

#include <ClassOne.h>

thread_local TFile   *JEventSource_jreader::mInputFile = 0;
thread_local TTree   *JEventSource_jreader::mInputTree = 0;

thread_local double   JEventSource_jreader::mValue     = 0.0;
thread_local XEvent *JEventSource_jreader::mEvent     = 0;

std::mutex JEventSource_jreader::mInputRootMutex;

// -------------------------------------------------------------------------------------

void JEventSource_jreader::Open(void)
{
  SetNumEventsToGetAtOnce(InputQueueDepth, InputQueueDepth);

  if (UseCheckpoint) 
    rename(_OUTPUT_ROOT_FILE_, _CHECKPOINT_ROOT_FILE_);
  else
    // For clarity;
    unlink(_CHECKPOINT_ROOT_FILE_);
} // JEventSource_jreader::Open()

// -------------------------------------------------------------------------------------

void JEventSource_jreader::GetEvent(std::shared_ptr<JEvent> jevent)
{
  // Initialize once;
  if (!mInputFile) {
    mInputFile = new TFile(WithProcessor && UseCheckpoint ? 
			   _CHECKPOINT_ROOT_FILE_ : _INPUT_ROOT_FILE_);
    
    mInputTree = dynamic_cast<TTree*>(mInputFile->Get("tree")); assert(mInputTree);
    
    // Either a JEvent dump or a single double value;
    if (WithProcessor && UseCheckpoint) 
      mInputTree->SetBranchAddress("event", &mEvent);
    else
      mInputTree->SetBranchAddress("value", &mValue);
  } //if

  {
    std::lock_guard<std::mutex> lock(/*GlobalRootLockRequired() ? GlobalRootMutex :*/ mInputRootMutex);

    jevent->SetEventNumber(mEvCurrent);

    // The first thread reaching this point takes care;
    if (!mEventsTotal) mEventsTotal = Statistics ? Statistics : mInputTree->GetEntries();

    if (++mEvCurrent >= mEventsTotal) throw JEventSource::RETURN_STATUS::kNO_MORE_EVENTS;

    mInputTree->GetEntry(mEvCurrent);
  }

  // Either populate the event with a ClassOne instance or call TH1D::Fill() directly;
  if (WithProcessor) {
    // Either use the input.root tree variable directly or extract it from the 
    // checkpoint.root event structure;
    if (UseCheckpoint) {
      // This call does not know anything about ClassOne, ClassTwo, etc;
      mEvent->ImportJEvent(jevent);

      // Do not allow ROOT to clean up the event pointers; FIXME: do it better later;
      mEvent = 0;
    } 
    else 
      jevent->InsertXObject(new ClassOne(mValue), "value");
  }
  else    
    JEventProcessor_jreader::FillThreadHistogram(mValue);
} // JEventSource_jreader::GetEvent()

// -------------------------------------------------------------------------------------


