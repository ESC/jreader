
#include <map>
#include <vector>
#include <memory>

#include <TObject.h>

#include <JANA/JEvent.h>

#ifndef _XEVENT_
#define _XEVENT_

class XEvent: public TObject {
 public:
  XEvent() {};
  XEvent(const std::shared_ptr<const JEvent>& aEvent) {
    auto factories = aEvent->GetFactorySet()->GetFactories();
    
    for(auto it: factories) {
      auto *factory = it.second;
      
      auto data = factory->GetXObjects();
      //printf("%s %s -> %lu; %8X %7lu\n", it.first.first.name(), it.first.second.c_str(), 
      // data->size(), (unsigned)pthread_self(), aEvent->GetEventNumber());
      // Ignore those which do not produce objects inherited from XObject;
      if (data) {
	mFactoryData[it.first.second] = data;
	factory->ResetDataVector();
      } //if
    }
  };
  ~XEvent() {};

  // Yes, JObject rather than XObject pointers here;
  std::map<std::string, std::vector<JObject*> *> mFactoryData;
  
  void ImportJEvent(std::shared_ptr<JEvent> jevent) {
    // Create new factories matching the imported event contents; 
    for(auto it: mFactoryData) {
      // Follow the logic of JEvent.h; 
      auto factory = jevent->GetFactorySet()->GetFactory<XObject>(it.first);
      if (!factory) {
	factory = new JFactoryT<XObject>(GetDemangledName<XObject>(), it.first);
	factory->SetCreated(true);
	jevent->GetFactorySet()->Add(factory);
      } //if	
    } //for it
    
    // Copy over imported XEvent factory data into the JEvent;
    {
      auto factories = jevent->GetFactorySet()->GetFactories();
      
      for(auto it: factories) {
	auto *factory = it.second;
	
	factory->Set(*mFactoryData[it.first.second]);
      } //for it
    }
  } // XEvent::ImportJEvent()
  
  ClassDef(XEvent,1);
};

#endif
