
#include <TFile.h>
#include <TTree.h>
#include <TH1D.h>

int main( void )
{
  TFile fin("input.root"), fout("output.root", "RECREATE");
  auto tree = dynamic_cast<TTree*>(fin.Get("tree")); 

  double value;
  tree->SetBranchAddress("value", &value);

  auto h1 = new TH1D("h1", "h1", 1000, -10., 10.);

  // Loop through all events in a "usual" way;
  unsigned nEvents = tree->GetEntries();
  for(unsigned ev=0; ev<nEvents; ev++) {
    tree->GetEntry(ev);
    h1->Fill(value);
  } //for ev

  h1->Write(); fin.Close(); fout.Close();
} // main()
