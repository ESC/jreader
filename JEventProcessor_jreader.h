
#include <TTree.h>
#include <TH1D.h>

class TFile;

#ifndef _JEVENTPROCESSOR_JREADER_H_
#define _JEVENTPROCESSOR_JREADER_H_

#include <JANA/JEventProcessor.h>

#include <globals.h>
#include <XEvent.h>

class JEventProcessor_jreader: public JEventProcessor {
 public:
 JEventProcessor_jreader(): mOutputFile(0), mOutputTree(0), 
    mOutputQueueDepth(OutputQueueDepth), mEvent(0) {};

  void Init(void);

  static std::mutex mOutputRootMutex;

  static thread_local TH1D *mOutputHist;

  static void InitializeLocalThreadHistogram( void );
  static void FillThreadHistogram(double value);

  void FlushOutputQueue(std::vector<XEvent> *queue);
  void Process(const std::shared_ptr<const JEvent>& aEvent);

  void Finish(void); 

  TFile *mOutputFile;
  TTree *mOutputTree;
  static TList *mList;

  unsigned mOutputQueueDepth;
  static thread_local std::vector<XEvent> *mOutputQueue;
  static std::vector<std::vector<XEvent>*> mOutputQueueArray;

  XEvent *mEvent;
};

#endif 

