
#include <TFile.h>
#include <TTree.h>
#include <TH1D.h>

#include <XEvent.h>
#include <ClassTwo.h>

int main( void )
{
  TFile fin("output.root"), fout("q.root", "RECREATE");
  auto tree = dynamic_cast<TTree*>(fin.Get("tree")); 

  double value;
  tree->SetBranchAddress("value", &value);

  auto hv = new TH1D("hv", "hv", 1000, -10., 10.);

  XEvent *event = 0;
  tree->SetBranchAddress("event", &event);

  // Loop through all events in a "usual" way;
  unsigned nEvents = tree->GetEntries();
  for(unsigned ev=0; ev<nEvents; ev++) {
    tree->GetEntry(ev);
    auto fdata = event->mFactoryData["ClassTwo-@*@-other"];
    hv->Fill(dynamic_cast<const ClassTwo*>((*fdata)[0])->GetValue(0));

    //hv->Fill(value);
  } //for ev

  hv->Write(); fin.Close(); fout.Close();
} // main()
