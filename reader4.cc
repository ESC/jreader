
#include <TFile.h>
#include <ROOT/RDataFrame.hxx>

int main( void )
{
  ROOT::EnableImplicitMT(4);

  TFile fout("output.root", "RECREATE");

  ROOT::RDataFrame rdf("tree", "input.root");
  auto h1 = rdf.Histo1D({"h1", "h1", 1000, -10., 10.}, "value");

  fout.cd(); h1->Write(); fout.Close();
} // main()
