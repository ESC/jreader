
#include <TObject.h>

#include <JANA/JObject.h>

#ifndef _CLASS_ONE_
#define _CLASS_ONE_

class ClassOne: public XObject , public TObject {
 public:
 ClassOne(): mValue(0.0) {};
 ClassOne(double value): mValue(value) {};

  double GetValue( void ) const { return mValue; }

 private:
  double mValue;

  ClassDef(ClassOne,1);
};

#endif
