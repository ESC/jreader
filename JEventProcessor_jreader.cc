
#include <TFile.h>

#include <JEventProcessor_jreader.h>

#include <ClassOne.h>
#include <ClassTwo.h>

std::mutex JEventProcessor_jreader::mOutputRootMutex;

TList *JEventProcessor_jreader::mList = new TList();
thread_local TH1D  *JEventProcessor_jreader::mOutputHist = 0;

std::vector<std::vector<XEvent>*> JEventProcessor_jreader::mOutputQueueArray;
thread_local std::vector<XEvent> *JEventProcessor_jreader::mOutputQueue = 
  new std::vector<XEvent>;

// -------------------------------------------------------------------------------------

void JEventProcessor_jreader::InitializeLocalThreadHistogram( void )
{
  TString hh; hh.Form("hvl%08u", (unsigned)pthread_self());
  mOutputHist = new TH1D(hh.Data(), "", 1000, -10., 10.);
    
  // Basically the only place, which requires an extra lock during initialization;
  {
    std::lock_guard<std::mutex> lock(mOutputRootMutex);
    
    mList->Add(mOutputHist);
    mOutputQueueArray.push_back(mOutputQueue);
  }
} // JEventProcessor_jreader::InitializeLocalThreadHistogram()

// -------------------------------------------------------------------------------------

void JEventProcessor_jreader::FillThreadHistogram(double value) {
  if (!mOutputHist) InitializeLocalThreadHistogram();
  
  mOutputHist->Fill(value);
} // JEventProcessor_jreader::FillThreadHistogram()

// -------------------------------------------------------------------------------------

void JEventProcessor_jreader::Init(void) {
  mOutputFile = new TFile(_OUTPUT_ROOT_FILE_, "RECREATE");

  if (WithProcessor) {
    mOutputTree = new TTree("tree", "tree");

    // A branch saving the current state of the JEvent container;
    mOutputTree->Branch("event", &mEvent);
  } //if
} // JEventProcessor_jreader::Init()

// -------------------------------------------------------------------------------------

void JEventProcessor_jreader::Process(const std::shared_ptr<const JEvent>& aEvent) {
  // Do nothing in '-Pwith-processor=no' mode (default);
  if (!WithProcessor) return;

  // Imitate CPU activity;
  if (FakeLoad) {
    struct timespec fake = {0, FakeLoad};
    nanosleep(&fake, 0);
  } //if

  {
    // Get (presumably a single) ClassOne instance;
    auto *md = aEvent->GetSingleXObject<ClassOne>("value");
    FillThreadHistogram(md->GetValue());
    
    // Create a duplicate as ClassTwo; create an associative link;
    if (!UseCheckpoint) {
      auto two = new ClassTwo(md->GetValue());
      aEvent->InsertXObject(two, "other");
      two->AddAssociatedObject(md);
    } //if
  }
  
  // XEvent ctor does not know anything about the actual aEvent contents;
  // it just loops through all the factories and makes a snapshot of the tagged 
  // JFactoryT::mData vector contents; then resets JFactoryT::mData in order to 
  // be able to keep ownership to all the pointers;
  mOutputQueue->push_back(XEvent(aEvent));

  if (mOutputQueue->size() == mOutputQueueDepth) FlushOutputQueue(mOutputQueue);
} // JEventProcessor_jreader::Process()

// -------------------------------------------------------------------------------------

void JEventProcessor_jreader::FlushOutputQueue(std::vector<XEvent> *queue) {
  std::lock_guard<std::mutex> lock(/*GlobalRootLockRequired() ? GlobalRootMutex :*/ mOutputRootMutex);
  
  for(auto event: *queue) {
    mEvent = &event;
    mOutputTree->Fill();
    
    // Do not care about memory clean up to the moment;
    //event->mFactoryData.clear();
  } 
  
  queue->clear();
} // JEventProcessor_jreader::FlushOutputQueue()

// -------------------------------------------------------------------------------------

void JEventProcessor_jreader::Finish(void) {
  auto hsum = new TH1D("h1", "h1", 1000, -10., 10.);

  hsum->Merge(mList); mList->Delete();

  if (WithProcessor) {
    // Flush thread queues;
    for(auto queue: mOutputQueueArray) 
      FlushOutputQueue(queue);
  
    mOutputTree->Write(); 
  } //if

  hsum->Write(); mOutputFile->Close(); 
} // JEventProcessor_jreader::Finish()

// -------------------------------------------------------------------------------------
