
#include <TROOT.h>

#include <JANA/JEventSourceGeneratorT.h>

#include "JEventProcessor_jreader.h"
#include "JEventSource_jreader.h"

// Prefer just to make them global variables;
unsigned Statistics       = 0;
bool WithProcessor        = false;
bool UseCheckpoint        = false;
unsigned InputQueueDepth  = 1;
unsigned OutputQueueDepth = 1;
unsigned ThreadCount      = 1;
unsigned FakeLoad         = 0;

// In the check-pointed mode there is an apparent conflict between reading events 
// from the input tree and writing them to the output tree, which only manifests
// itself in a multi-threaded mode with MyEvent entries imported; apply a global 
// lock for the time being;
//std::mutex GlobalRootMutex;

// -------------------------------------------------------------------------------------

//bool GlobalRootLockRequired( void )
//{
//return false;//UseCheckpoint && ThreadCount > 1;
//} // GlobalRootLockRequired()

// -------------------------------------------------------------------------------------

static void ParseYesNoParameter(JParameterManager *pm, const char *key, bool &variable)
{
  if (!pm->Exists(key)) return;

  {
    std::string str;
    pm->GetParameter(key,  str);
    // Do not overcomplicate things: everything which is not "yes" means "no";
    variable = (str == "yes") ? true : false;
  }
} // ParseYesNoParameter()

// -------------------------------------------------------------------------------------

extern "C"{
void InitPlugin(JApplication *app) {
  ROOT::EnableThreadSafety();

  InitJANAPlugin(app);
	
  // Add source generator
  app->Add( new JEventSourceGeneratorT<JEventSource_jreader>() );
  
  {
    JParameterManager *pm = app->GetJParameterManager();

    ParseYesNoParameter(pm, "with-processor", WithProcessor);
    ParseYesNoParameter(pm, "use-checkpoint", UseCheckpoint);

    if (pm->Exists("statistics"))         pm->GetParameter("statistics",         Statistics);
    if (pm->Exists("fake-cpu-load"))      pm->GetParameter("fake-cpu-load",      FakeLoad);
    if (pm->Exists("nthreads"))           pm->GetParameter("nthreads",           ThreadCount);
    if (pm->Exists("input-queue-depth"))  pm->GetParameter("input-queue-depth",  InputQueueDepth);
    if (pm->Exists("output-queue-depth")) pm->GetParameter("output-queue-depth", OutputQueueDepth);
  }

  // Add event processor anyway, since it is managing the output as well;
  app->Add( new JEventProcessor_jreader() ); 
}
} // "C"

// -------------------------------------------------------------------------------------
