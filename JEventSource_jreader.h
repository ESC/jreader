
#ifndef _JEVENT_SOURCE_JREADER_
#define _JEVENT_SOURCE_JREADER_

#include <JANA/JApplication.h>
#include <JANA/JEventSource.h>
#include <JANA/JEvent.h>

#include <TFile.h>
#include <TTree.h>

#include <XEvent.h>

class JEventSource_jreader: public JEventSource {
 public:
  // Constructor must take string and JApplication pointer as arguments
 JEventSource_jreader(std::string source_name, JApplication *app): 
  JEventSource(source_name, app), mEvCurrent(-1), mEventsTotal(0) {}
  virtual ~JEventSource_jreader() {}

  static std::mutex mInputRootMutex;

  // A description of this source type must be provided as a static member
  static std::string GetDescription(void) { return "Event source for jreader example"; }
  
  void Open(void);
  void GetEvent(std::shared_ptr<JEvent>);

  // These variables do not need to be thread local in present Jana implementation
  // (since GetEvent() is called in a single-threaded way), but it does not hurt 
  // to make them thread local either; initialization then naturally moves from 
  // Open() to GetEvent();
  static thread_local TFile *mInputFile;
  static thread_local TTree *mInputTree;
  static thread_local double mValue;
  static thread_local XEvent *mEvent;

  int mEvCurrent, mEventsTotal;
};

#endif
