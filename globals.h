
#ifndef _JREADER_GLOBALS_
#define _JREADER_GLOBALS_

// Well, this is used in the jreader plugin only; also do not bother
// to #define "tree", "value", "event", "h1", etc; who cares; 
#define _INPUT_ROOT_FILE_      (     "input.root")
#define _OUTPUT_ROOT_FILE_     (    "output.root")
#define _CHECKPOINT_ROOT_FILE_ ("checkpoint.root")

extern unsigned Statistics;

extern bool WithProcessor;
extern bool UseCheckpoint;

extern unsigned InputQueueDepth;
extern unsigned OutputQueueDepth;

extern unsigned FakeLoad;

//extern std::mutex GlobalRootMutex;
//extern bool GlobalRootLockRequired( void );

#endif
